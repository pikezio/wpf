﻿using Emgu.CV;
using PropertyChanged;
using System.Windows.Input;
using WPFtest.Commands;
using WPFtest.Models;
using WPFtest.Utils;

namespace WPFtest.ViewModels
{
    internal class ImageViewModel : BaseViewModel
    {
        const string SAVE_FILE_NAME = "config.xml";

        [OnChangedMethod(nameof(SaveFilterChanges))]
        public int BlurAmount { get; set; }

        [OnChangedMethod(nameof(SaveFilterChanges))]
        public int ThresholdAmount { get; set; }

        [OnChangedMethod(nameof(SaveFilterChanges))]
        public int SharpnessAmount { get; set; }

        [OnChangedMethod(nameof(SaveFilterChanges))]
        public bool Grayscale { get; set; }
        public Mat Image { get; set; }
        public DisplayModel DisplayModel { get; set; }
        public ICommand UploadPictureCommand { get; set; }
        public ICommand GrayscaleCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand NextMarkerCommand { get; set; }
        public ICommand PreviousMarkerCommand { get; set; }

        [OnChangedMethod(nameof(SavePropertyChanges))]
        public int ShowedMarkedId { get; set; }
        public string SelectedMarkerLabel => $"{ShowedMarkedId}/{ArucoDetection.GetDictionarySize()}";

        [OnChangedMethod(nameof(SavePropertyChanges))]
        public Mat SelectedMarker { get; set; } = ArucoDetection.GetMarker(0);
        [OnChangedMethod(nameof(SavePropertyChanges))]
        public string Description { get; set; }
        public bool SlidersEnabled => DisplayModel != null;

        public ImageViewModel()
        {
            UploadPictureCommand = new BaseCommand(o => UploadImageAction());
            GrayscaleCommand = new BaseCommand(state => ToggleGrayscale(state), o => { return DisplayModel != null; });
            SaveCommand = new BaseCommand(o => XmlSaveAndLoad.Save(SAVE_FILE_NAME, DisplayModel), o => { return DisplayModel != null; });
            LoadCommand = new BaseCommand(o => LoadSavedData(), o => { return DisplayModel != null; });
            NextMarkerCommand = new BaseCommand(o => GetNextMarker(), o => { return DisplayModel != null; });
            PreviousMarkerCommand = new BaseCommand(o => GetPreviousMarker(), o => { return DisplayModel != null && ShowedMarkedId != 0; });
        }

        private void GetNextMarker()
        {
            SelectedMarker = ArucoDetection.GetMarker(++ShowedMarkedId);
        }

        private void GetPreviousMarker()
        {
            if(ShowedMarkedId > 0)
                SelectedMarker = ArucoDetection.GetMarker(--ShowedMarkedId);
        }

        public void UploadImageAction()
        {
            DisplayModel = new DisplayModel(Files.OpenImageFile());
            Image = DisplayModel.Image;
        }

        public void SavePropertyChanges()
        {
            DisplayModel.Description = Description;
            DisplayModel.SelectedMarkerId = ShowedMarkedId;
        }

        public void SaveFilterChanges()
        {
            DisplayModel.ThresholdAmount = ThresholdAmount;
            DisplayModel.BlurAmount = BlurAmount;
            DisplayModel.Grayscale = Grayscale;
            DisplayModel.SharpnessAmount = SharpnessAmount;
            DisplayModel.Image = ImageProcessing.ApplyFilters(DisplayModel);
            Image = DisplayModel.Image;
        }

        public void ToggleGrayscale(object state)
        {
            if ((bool)state)
                Grayscale = true;
            else
                Grayscale = false;
        }

        public void LoadSavedData()
        {
            DisplayModel saved = XmlSaveAndLoad.Load(SAVE_FILE_NAME, DisplayModel);
            ThresholdAmount = saved.ThresholdAmount;
            BlurAmount = saved.BlurAmount;
            Grayscale = saved.Grayscale;
            Description = saved.Description;
            SharpnessAmount = saved.SharpnessAmount;
            ShowedMarkedId = saved.SelectedMarkerId;
            SelectedMarker = ArucoDetection.GetMarker(ShowedMarkedId);
            SaveFilterChanges();
        }
    }
}
