﻿using Emgu.CV;
using System;
using System.Windows;
using System.Windows.Input;
using WPFtest.Commands;
using WPFtest.Models;
using WPFtest.Utils;

namespace WPFtest.ViewModels
{
    public enum Mode
    {
        Waiting,
        ReadyToTakePicture,
        ReadyToCalibrate,
        Calibrating,
        Calibrated,
        Aruco
    }

    internal class VideoViewModel : BaseViewModel
    {
        public VideoFrameModel VideoFrameModel { get; set; }
        public Mode Mode { get; set; } = Mode.Waiting;
        private int _imagesTaken { get; set; } = 0;
        public string ImagesTaken => $"Take image {_imagesTaken}/{Calibration.TOTAL_IMAGE_CAPACITY}";
        public bool ShowCalibrationUI => Mode == Mode.Aruco ? false : true;
        public bool ShowCalibrateAgain => !ShowCalibrationUI;

        private Core _core;
        public ICommand CaptureFrameCommand { get; set; }
        public ICommand CalibrateCameraCommand { get; set; }
        public ICommand CalibrateAgainCommand { get; set; }
        public VideoViewModel()
        {
            _core = new Core(this);
            _core.StartVideoCapture();
            CalibrateCameraCommand = new BaseCommand(o => _core.CalibrateCamera(), o => { return Mode == Mode.ReadyToCalibrate; });
            CalibrateAgainCommand = new BaseCommand(o => _core.CalibrateAgain(), o => { return Mode == Mode.Aruco; });
            CaptureFrameCommand = new BaseCommand(o => _imagesTaken = _core.TakePicture(), o => {
                return Mode == Mode.ReadyToTakePicture && VideoFrameModel != null && VideoFrameModel.ChessboardDetected;
            });
        }

        internal void SetDrawingData(DisplayModel displayModel)
        {
            _core.SetDrawingData(displayModel);
        }
    }
}
