﻿using WPFtest.Commands;

namespace WPFtest.ViewModels
{
    internal class MainViewModel : BaseViewModel
    {
        public BaseCommand ImageViewCommand { get; set; }
        public BaseCommand ArucoViewCommand { get; set; }
        public BaseCommand CalibrationViewCommand { get; set; }

        private object _currentView;
        private ImageViewModel _imageViewModel;
        private VideoViewModel _videoViewModel;

        public object CurrentView
        {
            get { return _currentView; }
            set { _currentView = value; }
        }

        public MainViewModel()
        {
            _imageViewModel = new ImageViewModel();
            _videoViewModel = new VideoViewModel();
            CurrentView = _imageViewModel;

            ImageViewCommand = new BaseCommand(o =>
            {
                CurrentView = _imageViewModel;
            });

            ArucoViewCommand = new BaseCommand(o =>
            {
                CurrentView = _videoViewModel;
                _videoViewModel.SetDrawingData(_imageViewModel.DisplayModel);
            });
        }
    }
}
