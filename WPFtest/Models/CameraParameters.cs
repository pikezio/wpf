﻿using Emgu.CV;

namespace WPFtest.Utils
{
    public class CameraParameters
    {
        public CameraParameters()
        {

        }
        public CameraParameters(Mat cameraMatrix, Mat distCoeffs, Mat[] rvecs, Mat[] tvecs)
        {
            CameraMatrix = cameraMatrix;
            DistCoeffs = distCoeffs;
            Rvecs = rvecs;
            Tvecs = tvecs;
        }

        public CameraParameters(Mat cameraMatrix, Mat distCoeffs)
        {
            CameraMatrix = cameraMatrix;
            DistCoeffs = distCoeffs;
        }

        public Mat CameraMatrix { get; set; }
        public Mat DistCoeffs { get; set; }
        public Mat[] Rvecs { get; set; }
        public Mat[] Tvecs { get; set; }
    }
}
