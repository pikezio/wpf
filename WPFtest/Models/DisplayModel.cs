﻿using Emgu.CV;
using System;

namespace WPFtest.Models
{
    public class DisplayModel
    {
        // Current values for displayed image
        public Mat Image { get; set; }
        public int BlurAmount { get; set; } = 0;
        public int ThresholdAmount { get; set; } = 0;
        public int SharpnessAmount { get; set; } = 0;
        public bool Grayscale { get; set; } = false;
        public string Description { get; set; }
        public int SelectedMarkerId { get; set; }
        public DisplayModel()
        {

        }

        // Original unchanged image
        private Mat OriginalImage;

        public DisplayModel(string filePath)
        {
            if (String.IsNullOrEmpty(filePath))
            {
                OriginalImage = new Mat();
            }
            OriginalImage = new Mat(filePath);
            ResetToOriginalImage();
        }

        public Mat ResetToOriginalImage()
        {
            if (OriginalImage == null)
                return new Mat();
            
            Image = OriginalImage;
            return OriginalImage;
        }
    }
}
