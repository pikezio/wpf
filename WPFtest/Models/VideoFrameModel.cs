﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFtest.Models
{
    internal class VideoFrameModel
    {
        public VideoFrameModel(Mat image, bool chessboardDetected = false)
        {
            Image = image;
            ChessboardDetected = chessboardDetected;
        }

        public Mat Image { get; set; }
        public bool ChessboardDetected { get; set; }
    }
}
