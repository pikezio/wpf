﻿using Emgu.CV;
using System;
using System.Windows.Input;
using System.Windows.Interop;
using WPFtest.Models;
using WPFtest.ViewModels;

namespace WPFtest.Utils
{
    internal class Core
    {
        private Calibration _calibration;
        private VideoCapture _capture;
        private ArucoDetection _aruco;
        private VideoViewModel _videoViewModel;
        private CameraParameters _cameraParameters;

        DisplayModel _displayModel;

        public Core(VideoViewModel videoViewModel)
        {
            _videoViewModel = videoViewModel;
            _calibration = new Calibration();
        }
        public void StartVideoCapture()
        {
            _videoViewModel.Mode = Mode.ReadyToTakePicture;
            _capture = new VideoCapture(0);

            // Check if calibration data exists already
            var calibrationData = GetCalibrationData();
            if (calibrationData != null)
            {
                _cameraParameters = calibrationData;
                _videoViewModel.Mode = Mode.Aruco;
            }
            ComponentDispatcher.ThreadIdle += new EventHandler(ProcessFrame);
        }

        public void SetDrawingData(DisplayModel displayModel)
        {
            _displayModel = displayModel;
        }

        public void StopVideoCapture()
        {
            _capture.Dispose();
            
        }

        internal void CalibrateCamera()
        {
            _videoViewModel.Mode = Mode.Calibrating;
            _cameraParameters = _calibration.CalibrateCamera();
            SaveToFile(_cameraParameters);
            _videoViewModel.Mode = Mode.Aruco;
        }

        public int TakePicture()
        {
            int pictureCount = _calibration.TakePicture();
            if (pictureCount == Calibration.TOTAL_IMAGE_CAPACITY) _videoViewModel.Mode = Mode.ReadyToCalibrate;
            return pictureCount;
        }

        internal void CalibrateAgain()
        {
            _calibration = new Calibration();
            _cameraParameters = null;
            _videoViewModel.Mode = Mode.ReadyToTakePicture;
        }

        public void ProcessFrame(object? sender, EventArgs e)
        {
            VideoFrameModel frame = new VideoFrameModel(_capture.QueryFrame());
            _videoViewModel.VideoFrameModel = frame;
            switch (_videoViewModel.Mode)
            {
                case Mode.ReadyToTakePicture:
                    _videoViewModel.VideoFrameModel = _calibration.DetectChessboard(frame);
                    CommandManager.InvalidateRequerySuggested();
                    break;
                case Mode.Aruco:
                    _aruco = new ArucoDetection(_cameraParameters);
                    VideoFrameModel corrected = _calibration.RemoveDistortion(frame, _cameraParameters);
                    _videoViewModel.VideoFrameModel = new VideoFrameModel(_aruco.Detect(corrected.Image, _displayModel));
                    break;
            }
        }

        private static void SaveToFile(CameraParameters parameters)
        {
            try
            {
                FileStorage fs = new FileStorage("cameraParameters.xml", FileStorage.Mode.Write);
                fs.Write(parameters.CameraMatrix, "cameraMatrix");
                fs.Write(parameters.DistCoeffs, "distCoeffs");
            }
            catch (Exception error)
            {
                System.Windows.MessageBox.Show(error.ToString(), "Error",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

        public CameraParameters GetCalibrationData()
        {
            try
            {
                FileStorage fs = new FileStorage("cameraParameters.xml", FileStorage.Mode.Read);
                Mat cameraMatrix = new Mat();
                Mat distCoeffs = new Mat();
                fs.GetNode("cameraMatrix").ReadMat(cameraMatrix);
                fs.GetNode("distCoeffs").ReadMat(distCoeffs);
                if(!cameraMatrix.IsEmpty && !distCoeffs.IsEmpty)
                    return new CameraParameters(cameraMatrix, distCoeffs);
                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
