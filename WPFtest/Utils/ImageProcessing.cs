﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using WPFtest.Models;

namespace WPFtest.Utils
{
    internal static class ImageProcessing
    {
        /// <summary>
        /// Applies filters with specific properties from this class
        /// </summary>
        public static Mat ApplyFilters(DisplayModel displayModel)
        {
            displayModel.ResetToOriginalImage();
            Mat output = displayModel.Image;

            output = ApplyBlur(output, displayModel);

            output = ApplyThreshold(output, displayModel);

            output = ApplySharpness(output, displayModel);

            output = ApplyGrayscale(output, displayModel);

            return output;
        }

        private static Mat ApplySharpness(Mat input, DisplayModel displayModel)
        {
            Mat output = input.Clone();
            using Mat blur = new Mat();
            if(displayModel.SharpnessAmount > 0)
            {
                if(displayModel.SharpnessAmount % 2 != 0)
                    CvInvoke.GaussianBlur(input, blur, new Size(displayModel.SharpnessAmount, displayModel.SharpnessAmount), 0);
                else
                    CvInvoke.GaussianBlur(input, blur, new Size(displayModel.SharpnessAmount - 1, displayModel.SharpnessAmount - 1), 0);
                CvInvoke.AddWeighted(output, 1.5, blur, -0.5, 0, output);
            }
            return output;
        }

        private static Mat ApplyBlur(Mat input, DisplayModel displayModel)
        {
            Mat output = input.Clone();

            // Blur
            if (displayModel.BlurAmount > 0)
                CvInvoke.Blur(input, output, new Size(displayModel.BlurAmount, displayModel.BlurAmount), new Point());

            return output;
        }

        private static Mat ApplyThreshold(Mat input, DisplayModel displayModel)
        {
            Mat output = input.Clone();

            // Threshold
            if (displayModel.ThresholdAmount > 0)
                CvInvoke.Threshold(input, output, displayModel.ThresholdAmount, 255, Emgu.CV.CvEnum.ThresholdType.Binary);

            return output;
        }

        private static Mat ApplyGrayscale(Mat input, DisplayModel displayModel)
        {
            if (displayModel.Grayscale)
                return input.ToImage<Gray, Byte>().Mat;
            return input;
        }
    }
}
