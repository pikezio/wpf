﻿using System.IO;
using System.Xml.Serialization;
using WPFtest.Models;

namespace WPFtest.Utils
{
    internal static class XmlSaveAndLoad
    {
        public static void Save(string fileName, DisplayModel imageModel)
        {
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var xmlSerializer = new XmlSerializer(typeof(DisplayModel));
                xmlSerializer.Serialize(stream, imageModel);
            }
        }

        public static DisplayModel? Load(string fileName, DisplayModel imageModel)
        {
            DisplayModel dataToLoad;
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var xmlSerializer = new XmlSerializer(typeof(DisplayModel));
                dataToLoad = (DisplayModel)xmlSerializer.Deserialize(stream);
            }
            return dataToLoad;
        }
    }
}
