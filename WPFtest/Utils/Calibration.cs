﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using WPFtest.Models;

namespace WPFtest.Utils
{
    internal class Calibration
    {
        // CONSTANTS
        public static int TOTAL_IMAGE_CAPACITY = 15;
        const float BOARD_SQUARE_LENGTH = 2; // meters
        Size BOARD_SIZE = new Size(6, 9);

        public bool CanTakePicture => found;
        public Mat[] _rvecs, _tvecs;

        private bool found = false;
        private List<Mat> _calibrationPictures = new(); // List of calibration pictures

        private VectorOfPointF _corners = new VectorOfPointF(); // chessboard corners
        private MCvPoint3D32f[][] _cornersObjectList;
        private PointF[][] _cornersPointsList;
        private VectorOfPointF[] _cornersPointsVec;

        private Mat _frame = new Mat();
        private readonly Mat _grayFrame = new Mat();
        private readonly Mat _cameraMatrix = new Mat(3, 3, DepthType.Cv64F, 1);
        private readonly Mat _distCoeffs = new Mat(8, 1, DepthType.Cv64F, 1);

        private List<MCvPoint3D32f> createObjectList()
        {
            var objectList = new List<MCvPoint3D32f>();
            for (int i = 0; i < BOARD_SIZE.Height; i++)
            {
                for (int j = 0; j < BOARD_SIZE.Width; j++)
                {
                    objectList.Add(new MCvPoint3D32f(j * BOARD_SQUARE_LENGTH, i * BOARD_SQUARE_LENGTH, 0.0F));
                }
            }
            return objectList;
        }

        public CameraParameters CalibrateCamera()
        {
            _cornersObjectList = new MCvPoint3D32f[_calibrationPictures.Count][];
            _cornersPointsList = new PointF[_calibrationPictures.Count][];
            _cornersPointsVec = new VectorOfPointF[_calibrationPictures.Count];

            for (int k = 0; k < _calibrationPictures.Count; k++)
            {
                Mat? image = _calibrationPictures[k];
                _cornersPointsVec[k] = new VectorOfPointF();
                CvInvoke.FindChessboardCorners(image, BOARD_SIZE, _cornersPointsVec[k], CalibCbType.AdaptiveThresh | CalibCbType.FastCheck | CalibCbType.NormalizeImage);
                CvInvoke.CornerSubPix(image, _cornersPointsVec[k], new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));

                List<MCvPoint3D32f> objectList = createObjectList();

                _cornersObjectList[k] = objectList.ToArray();
                _cornersPointsList[k] = _cornersPointsVec[k].ToArray();
            }

            double error = CvInvoke.CalibrateCamera(_cornersObjectList, _cornersPointsList, _grayFrame.Size,
                     _cameraMatrix, _distCoeffs, CalibType.RationalModel, new MCvTermCriteria(30, 0.1), out _rvecs, out _tvecs);

            System.Windows.MessageBox.Show(@"Intrinsic Calculation Error: " + error.ToString()); //display the results to the user
            var parameters = new CameraParameters(_cameraMatrix, _distCoeffs, _rvecs, _tvecs);
            return parameters;
        }

        public VideoFrameModel DetectChessboard(VideoFrameModel frameModel)
        {
            _frame = frameModel.Image;
            CvInvoke.CvtColor(_frame, _grayFrame, ColorConversion.Bgr2Gray);

            found = CvInvoke.FindChessboardCorners(_grayFrame, BOARD_SIZE, _corners, CalibCbType.AdaptiveThresh | CalibCbType.FastCheck | CalibCbType.NormalizeImage);

            if (found)
            {
                CvInvoke.CornerSubPix(_grayFrame, _corners, new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));
                CvInvoke.DrawChessboardCorners(_frame, BOARD_SIZE, _corners, found);
            }
            return new VideoFrameModel(_frame, found);
        }

        public VideoFrameModel RemoveDistortion(VideoFrameModel frame, CameraParameters parameters)
        {
            Mat output = frame.Image.Clone();
            CvInvoke.Undistort(frame.Image, output, parameters.CameraMatrix, parameters.DistCoeffs);
            return new VideoFrameModel(output);
        }

        public int TakePicture()
        {
            _calibrationPictures.Add(_grayFrame);
            return _calibrationPictures.Count;
        }
    }
}
