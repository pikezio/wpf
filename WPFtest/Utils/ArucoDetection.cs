﻿using Emgu.CV;
using Emgu.CV.Aruco;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using WPFtest.Models;

namespace WPFtest.Utils
{
    internal class ArucoDetection
    {
        const float ARUCO_MARKER_SIZE = 0.05f;
        private static readonly Dictionary ArucoDictionary = new Dictionary(Dictionary.PredefinedDictionaryName.Dict6X6_250);
        private DetectorParameters ArucoParameters = DetectorParameters.GetDefault();

        private VectorOfInt ids; // IDs of detected markers
        private VectorOfVectorOfPointF corners; // Corner points of detected markers
        private VectorOfVectorOfPointF rejected; // Rejected markers
        private CameraParameters _cameraParameters;

        public ArucoDetection(CameraParameters cameraParameters)
        {
            _cameraParameters = cameraParameters;
        }

        public static Mat GetMarker(int markerId)
        {
            Mat marker = new Mat();
            ArucoInvoke.DrawMarker(ArucoDictionary, markerId, 150, marker);
            return marker;
        }

        public static int GetDictionarySize()
        {
            return 250;
        }

        public Mat Detect(Mat frame, DisplayModel displayModel)
        {
            if(displayModel == null) return frame;

            ids = new VectorOfInt();
            corners = new VectorOfVectorOfPointF();
            rejected = new VectorOfVectorOfPointF();

            Mat rvecs = new Mat();
            Mat tvecs = new Mat();

            ArucoInvoke.DetectMarkers(frame, ArucoDictionary, corners, ids, ArucoParameters, rejected);

            if (ids.Size > 0)
            {
                ArucoInvoke.EstimatePoseSingleMarkers(corners, ARUCO_MARKER_SIZE, _cameraParameters.CameraMatrix, _cameraParameters.DistCoeffs,
                    rvecs, tvecs);
                for (int i = 0; i < ids.Size; i++)
                {
                    if(ids[i] == displayModel.SelectedMarkerId)
                    {
                        double textSize = Math.Round(1.0f / (double)tvecs.GetData().GetValue(0, 0, 2), 1);
                        frame = DrawImage(frame, displayModel.Image);
                        frame = DrawCustomMarkers(frame, displayModel.Description, textSize);
                        //ArucoInvoke.DrawDetectedMarkers(frame, corners, ids, new MCvScalar(255, 0, 255));
                        //using Mat rvecMat = rvecs.Row(i);
                        //using Mat tvecMat = tvecs.Row(i);
                        //ArucoInvoke.DrawAxis(frame, _cameraParameters.CameraMatrix, _cameraParameters.DistCoeffs, rvecMat, tvecMat, ARUCO_MARKER_SIZE * 0.5f);
                    }
                }
            }
            return frame;
        }

        public Mat DrawImage(Mat nextFrame, Mat imgToAdd)
        {
            Mat image = nextFrame.Clone();

            for (int i = 0; i < corners.Size; i++)
            {
                var topLeft = corners[i][0];
                var topRight = corners[i][1];
                var bottomLeft = corners[i][3];
                var bottomRight = corners[i][2];

                PointF[] imagePositions = new PointF[]
                {
                    new PointF(0, 0),
                    new PointF(imgToAdd.Width, 0),
                    new PointF(imgToAdd.Width,
                    imgToAdd.Height), new PointF(0, imgToAdd.Height)
                };
                PointF[] arucoPoints = new PointF[] { topLeft, topRight, bottomRight, bottomLeft };
                VectorOfPoint arucoPointsRound = new VectorOfPoint(new Point[] { Point.Round(topLeft), Point.Round(topRight), Point.Round(bottomRight), Point.Round(bottomLeft) });

                var transformMatrix = CvInvoke.FindHomography(imagePositions, arucoPoints);

                Mat imgOut = image.Clone();
                CvInvoke.WarpPerspective(imgToAdd, imgOut, transformMatrix, image.Size, Emgu.CV.CvEnum.Inter.Nearest);

                CvInvoke.FillConvexPoly(image, arucoPointsRound, new MCvScalar(0), Emgu.CV.CvEnum.LineType.AntiAlias);

                image += imgOut;
            }

            return image;
        }

        public Mat DrawCustomMarkers(Mat nextFrame, string description, double textSize)
        {
            double textScale = 0.3f;
            int textGapScale = 10;
            Mat image = nextFrame.Clone();

            for (int i = 0; i < corners.Size; i++)
            {
                var topLeft = Point.Round(corners[i][0]);
                var topRight = Point.Round(corners[i][1]);
                var bottomLeft = Point.Round(corners[i][3]);
                var bottomRight = Point.Round(corners[i][2]);

                double startingY = bottomLeft.Y + 25;
                using (StringReader reader = new StringReader(description))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var textLocation = new Point(bottomLeft.X, (int)startingY);
                        CvInvoke.PutText(image, line, textLocation, Emgu.CV.CvEnum.FontFace.HersheySimplex, textSize * textScale, new MCvScalar(0), 2, Emgu.CV.CvEnum.LineType.AntiAlias);
                        CvInvoke.PutText(image, line, textLocation, Emgu.CV.CvEnum.FontFace.HersheySimplex, textSize * textScale, new MCvScalar(255, 255, 255), 1, Emgu.CV.CvEnum.LineType.AntiAlias);
                        startingY += textSize * textGapScale; 
                    }
                }
            }

            return image;
        }


        private static void DebugPoints(PointF topLeft, PointF topRight, PointF bottomLeft, PointF bottomRight)
        {
            Debug.WriteLine("Top Left: " + topLeft);
            Debug.WriteLine("Top Right: " + topRight);
            Debug.WriteLine("Bottom Left: " + bottomLeft);
            Debug.WriteLine("Bottom Right: " + bottomRight);
        }
    }
}
