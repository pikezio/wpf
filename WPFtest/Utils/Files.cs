﻿using Microsoft.Win32;

namespace WPFtest.Utils
{
    static class Files
    {
        /// <summary>
        /// Opens a dialog to upload an image
        /// </summary>
        public static string OpenImageFile()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            var response = fileDialog.ShowDialog();
            if (response == true)
                return fileDialog.FileName;
            return "";
        }
    }
}
